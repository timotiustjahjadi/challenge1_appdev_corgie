//
//  HistoryCollectionViewController.swift
//  Corgi
//
//  Created by Christopher Kevin Susandji on 19/03/20.
//  Copyright © 2020 artitude. All rights reserved.
//

import UIKit

private let reuseIdentifier = "Cell"

class HistoryCollectionViewController: UICollectionViewController {

    @IBOutlet weak var deleteButton: UIBarButtonItem!
        
        
         let reuseIdentifier = "cell"
         var images = [#imageLiteral(resourceName: "1"), #imageLiteral(resourceName: "5"), #imageLiteral(resourceName: "3"), #imageLiteral(resourceName: "6"), #imageLiteral(resourceName: "2"), #imageLiteral(resourceName: "4"), #imageLiteral(resourceName: "2"), #imageLiteral(resourceName: "2"), #imageLiteral(resourceName: "2"), #imageLiteral(resourceName: "2")]
       
        
        
        override func viewDidLoad() {
            super.viewDidLoad()
            
            navigationItem.leftBarButtonItem = editButtonItem
            editButtonItem.tintColor = UIColor.link
            

            // Uncomment the following line to preserve selection between presentations
            // self.clearsSelectionOnViewWillAppear = false

            // Register cell classes
    //        self.collectionView!.register(UICollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)

            // Do any additional setup after loading the view.
        }

        /*
        // MARK: - Navigation

        // In a storyboard-based application, you will often want to do a little preparation before navigation
        override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
            // Get the new view controller using [segue destinationViewController].
            // Pass the selected object to the new view controller.
        }
        */

        // MARK: UICollectionViewDataSource

    //    override func numberOfSections(in collectionView: UICollectionView) -> Int {
    //        // #warning Incomplete implementation, return the number of sections
    //        return 1
    //    }


        override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            // #warning Incomplete implementation, return the number of items
            return self.images.count
        }

        override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath) as! HistoryCollectionViewCell
        
            // Configure the cell
            cell.backgroundColor = UIColor.white // make cell more visible in our example project
    //               cell.layer.borderColor = UIColor.black.cgColor
    //               cell.layer.borderWidth = 1
                   cell.layer.cornerRadius = 8
            cell.myImage.image = self.images[indexPath.row]
                  
        
            return cell
        }
         // MARK: Editing mode
        @IBAction func deleteItem(_ sender: Any) {
            
            if  let selectedCells = collectionView.indexPathsForSelectedItems {
                
                // The selected cells will be reversed and sorted so the items with the highest index will be removed first.
                let items = selectedCells.map { $0.item }.sorted().reversed()
                
                // the items will be removed from the modelData array
                for item in items {
                    images.remove(at: item)
                }
                
                // The selected cells will be deleted from the Collection View Controller and the trash icon will be disabled.
                collectionView.deleteItems(at: selectedCells)
                deleteButton.isEnabled = false
                
                
               
                
                
            }
        }
        
        // This method will check if a cell is in editing mode
        override func setEditing(_ editing: Bool, animated: Bool) {
            super.setEditing(editing, animated: animated)

            collectionView.allowsMultipleSelection = editing
            let indexPaths = collectionView.indexPathsForVisibleItems
            for indexPath in indexPaths {
                let cell = collectionView.cellForItem(at: indexPath) as! HistoryCollectionViewCell
                cell.isInEditingMode = editing
            }
        }
        // If a cell is selected the trash icon is displayed, otherwise it remains hidden
        override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            if !isEditing {
                deleteButton.isEnabled = false
            } else {
                deleteButton.isEnabled = true
            }
        }
        // if an cell is deselected and there are no other cells selected the trash icon is hidden.
        override func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
            
            if let selectedItems = collectionView.indexPathsForSelectedItems, selectedItems.count == 0 {
                deleteButton.isEnabled = false
            }
            else {
                deleteButton.isEnabled = true
            }
        }
        

        // MARK: UICollectionViewDelegate
        
        // change background color when user touches cell
        override func collectionView(_ collectionView: UICollectionView, didHighlightItemAt indexPath: IndexPath) {
            let cell = collectionView.cellForItem(at: indexPath)
    //        cell?.layer.borderWidth = 5
    //        cell?.layer.borderColor = UIColor.orange.cgColor
            cell?.alpha = 0.5

        }
        
        

        // change background color back when user releases touch
        override func collectionView(_ collectionView: UICollectionView, didUnhighlightItemAt indexPath: IndexPath) {
            let cell = collectionView.cellForItem(at: indexPath)
            cell?.backgroundColor = UIColor.white
    //        cell?.layer.borderColor = UIColor.clear.cgColor
            cell?.alpha = 1

        }
        
        

        /*
        // Uncomment this method to specify if the specified item should be highlighted during tracking
        override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
            return true
        }
        */

        /*
        // Uncomment this method to specify if the specified item should be selected
        override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
            return true
        }
        */

        /*
        // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
        override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
            return false
        }

        override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
            return false
        }

        override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
        
        }
        */

}
