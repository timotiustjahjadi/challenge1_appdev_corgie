//
//  ColorPickerViewController.swift
//  Corgi
//
//  Created by Christopher Kevin Susandji on 26/03/20.
//  Copyright © 2020 artitude. All rights reserved.
//

import UIKit

class ColorPickerViewController: UIViewController {

    //Main-Photo
        @IBOutlet weak var ChosenImage: UIImageView!
        //PickerA
        @IBOutlet weak var contain: UIView!
        @IBOutlet weak var PickerBG: UIImageView!
        //PickerB
        @IBOutlet weak var containB: UIView!
        @IBOutlet weak var PickerBBG: UIImageView!
        
        @IBOutlet weak var ColorA: UIImageView!
        @IBOutlet weak var ColorB: UIImageView!

        @IBOutlet weak var ColorALabel: UILabel!
        @IBOutlet weak var ColorBLabel: UILabel!
        
        @IBOutlet weak var ColorBLabelRGB: UILabel!
        @IBOutlet weak var ColorALabelRGB: UILabel!
    
        @IBOutlet weak var SaveButton: UIButton!
        
        var mainImage : UIImage? = nil
    
        var timer : Timer?
        
        @IBAction func SaveButtonAction(_ sender: Any) {
            timer!.invalidate()
            let alert = UIAlertController(title: "Saved!", message: "Now you can see what colors you have taken in the History tab.", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                print("complete")
            }))
            
            self.present(alert, animated: true)
        }
        
        override func viewDidLoad() {
            super.viewDidLoad()
            
            ChosenImage.image = mainImage
//            if let mainImage = mainImage {
//               makePretty(image : mainImage)
//            }
            
            contain.isUserInteractionEnabled = true
            containB.isUserInteractionEnabled = true
            
            let imageW = 414.0
            let imageH = 551.0
//            ChosenImage.image = UIImage(named: "imageyes")
            let images : UIImage = self.resizeImage(image: ChosenImage.image!, targetSize: CGSize(width: imageW, height: imageH))
            
            let image : UIImage = self.cropsToSquare(image: images, targetSize: CGSize(width: imageW, height: imageH))
            
            ChosenImage.image = image
            ChosenImage.contentMode =  UIView.ContentMode.scaleAspectFill
            
           timer = Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true, block: { [weak self] (_) in
                
                guard let strongSelf = self else {return}
                
                let gesture = UIPanGestureRecognizer(target: self, action: #selector(strongSelf.wasDragged(_:)))
                let gesture2 = UIPanGestureRecognizer(target: self, action: #selector(strongSelf.wasDragged2(_:)))
                
                strongSelf.contain.addGestureRecognizer(gesture)
                strongSelf.containB.addGestureRecognizer(gesture2)
                
                var frm : CGRect = strongSelf.contain.frame
                var frm1: CGRect = strongSelf.containB.frame
                
                func ConvertToHex(IntColor : Int, Switch : Bool) -> String{
                    let first = IntColor / 16
                    let last = IntColor - (first * 16)
                    
                    let letterfirst : String?
                    let letterlast : String?
                    
                    if Switch == true{
                        if first == 10{
                            letterfirst = "A"
                            return letterfirst ?? ""
                        }
                        else if first == 11{
                            letterfirst = "B"
                            return letterfirst ?? ""
                        }
                        else if first == 12{
                            letterfirst = "C"
                            return letterfirst ?? ""
                        }
                        else if first == 13{
                            letterfirst = "D"
                            return letterfirst ?? ""
                        }
                        else if first == 14{
                            letterfirst = "E"
                            return letterfirst ?? ""
                        }
                        else if first == 15{
                            letterfirst = "F"
                            return letterfirst ?? ""
                        }
                        else{
                            return String(first)
                        }
                    }
                    
                    if Switch == false{
                        if last == 10{
                            letterlast = "A"
                            return letterlast ?? ""
                        }
                        else if last == 11{
                            letterlast = "B"
                            return letterlast ?? ""
                        }
                        else if last == 12{
                            letterlast = "C"
                            return letterlast ?? ""
                        }
                        else if last == 13{
                            letterlast = "D"
                            return letterlast ?? ""
                        }
                        else if last == 14{
                            letterlast = "E"
                            return letterlast ?? ""
                        }
                        else if last == 15{
                            letterlast = "F"
                            return letterlast ?? ""
                        }
                        else{
                            return String(last)
                        }
                    }
                    return ""
                }
            
                //outOfBound-Handling
                if frm.origin.y >= 507.0{
                    frm.origin.y = 507.0
                    strongSelf.contain.frame.origin.y = 507.0
                }
                if frm1.origin.y >= 507.0 {
                    frm1.origin.y = 507.0
                    strongSelf.containB.frame.origin.y = 507.0
                }
                if frm.origin.y <= 0.0{
                    frm.origin.y = 0.0
                    strongSelf.contain.frame.origin.y = 0.0
                }
                if frm1.origin.y <= 0.0 {
                    frm1.origin.y = 0.0
                    strongSelf.containB.frame.origin.y = 0.0
                }
                if frm.origin.x <= 0.0 {
                    frm.origin.x = 0.0
                    strongSelf.contain.frame.origin.x = 0.0
                }
                if frm1.origin.x <= 0.0 {
                    frm1.origin.x = 0.0
                    strongSelf.containB.frame.origin.x = 0.0
                }
                
                strongSelf.ColorA.backgroundColor = image.pixelColor(x: Int(frm.origin.x), y: Int(frm.origin.y))
                strongSelf.ColorB.backgroundColor = image.pixelColor(x: Int(frm1.origin.x), y: Int(frm1.origin.y))
                           
                //colorA-RGB
                let colorAtA = String(describing: image.pixelColor(x: Int(frm.origin.x), y: Int(frm.origin.y)))
            
                let ArrayColorA = colorAtA.components(separatedBy: " ")
            
                let colorAtA_R = Float(ArrayColorA[1])
                let colorAtA_G = Float(ArrayColorA[2])
                let colorAtA_B = Float(ArrayColorA[3])
            
                let ColorARed = Float(colorAtA_R ?? 0.0) * 255
                let ColorAGreen = Float(colorAtA_G ?? 0.0) * 255
                let ColorABlue = Float(colorAtA_B ?? 0.0) * 255
                
                let IntColorARed = Int(ColorARed)
                let IntColorAGreen = Int(ColorAGreen)
                let IntColorABlue = Int(ColorABlue)
            
                let IntColorRedFirst1 = ConvertToHex(IntColor: IntColorARed, Switch: true)
                let IntColorRedLast1 = ConvertToHex(IntColor: IntColorARed, Switch: false)
                let IntColorAARed = IntColorRedFirst1 + IntColorRedLast1
                
                let IntColorGreenFirst1 = ConvertToHex(IntColor: IntColorAGreen, Switch: true)
                let IntColorGreenLast1 = ConvertToHex(IntColor: IntColorAGreen, Switch: false)
                let IntColorAAGreen = IntColorGreenFirst1 + IntColorGreenLast1
                
                let IntColorBlueFirst1 = ConvertToHex(IntColor: IntColorABlue, Switch: true)
                let IntColorBlueLast1 = ConvertToHex(IntColor: IntColorABlue, Switch: false)
                let IntColorAABlue = IntColorBlueFirst1 + IntColorBlueLast1
                
                self?.ColorALabelRGB.text = "#" + IntColorAARed + IntColorAAGreen + IntColorAABlue
                
    //            self?.ColorALabelRGB.text = "R: " + String(Int(ColorARed)) + " G: " + String(Int(ColorAGreen)) + " B: " + String(Int(ColorABlue))
            
            //ColorALabel-Condition
            if (ColorARed >= 0.0 && ColorARed <= 40.0) && (ColorAGreen >= 0.0 && ColorAGreen <= 40.0) && (ColorABlue >= 0.0 && ColorABlue <= 40.0){
                self?.ColorALabel.text = "Black"
            }
            if (ColorARed >= 200.0 && ColorARed <= 255.0) && (ColorAGreen >= 200.0 && ColorAGreen <= 255.0) && (ColorABlue >= 200.0 && ColorABlue <= 255.0){
                self?.ColorALabel.text = "White"
            }
            if (ColorARed >= 180.0 && ColorARed <= 255.0) && (ColorAGreen >= 0.0 && ColorAGreen <= 80.0) && (ColorABlue >= 0.0 && ColorABlue <= 80.0){
                self?.ColorALabel.text = "Red"
            }
            if (ColorARed >= 140.0 && ColorARed <= 180.0) && (ColorAGreen >= 0.0 && ColorAGreen <= 80.0) && (ColorABlue >= 100.0 && ColorABlue <= 180.0){
                self?.ColorALabel.text = "Red Violet"
            }
            if (ColorARed >= 100.0 && ColorARed <= 140.0) && (ColorAGreen >= 0.0 && ColorAGreen <= 80.0) && (ColorABlue >= 100.0 && ColorABlue <= 140.0){
                self?.ColorALabel.text = "Violet"
            }
            if (ColorARed >= 0.0 && ColorARed <= 40.0) && (ColorAGreen >= 0.0 && ColorAGreen <= 40.0) && (ColorABlue >= 150.0 && ColorABlue <= 255){
                self?.ColorALabel.text = "Blue"
            }
            if (ColorARed >= 0.0 && ColorARed <= 80.0) && (ColorAGreen >= 100.0 && ColorAGreen <= 180.0) && (ColorABlue >= 100.0 && ColorABlue <= 180.0){
                self?.ColorALabel.text = "Blue Green"
            }
            if (ColorARed >= 0.0 && ColorARed <= 80.0) && (ColorAGreen >= 100.0 && ColorAGreen <= 140.0) && (ColorABlue >= 0.0 && ColorABlue <= 80.0){
                self?.ColorALabel.text = "Green"
            }
            if (ColorARed >= 100.0 && ColorARed <= 180.0) && (ColorAGreen >= 100.0 && ColorAGreen <= 180.0) && (ColorABlue >= 0.0 && ColorABlue <= 80.0){
                self?.ColorALabel.text = "Yellow Green"
            }
            if (ColorARed >= 200.0 && ColorARed <= 255.0) && (ColorAGreen >= 200.0 && ColorAGreen <= 255.0) && (ColorABlue >= 0.0 && ColorABlue <= 80.0){
                self?.ColorALabel.text = "Yellow"
            }
            if (ColorARed >= 200.0 && ColorARed <= 255.0) && (ColorAGreen >= 100.0 && ColorAGreen <= 180.0) && (ColorABlue >= 0.0 && ColorABlue <= 80.0){
                self?.ColorALabel.text = "Orange"
            }
            if (ColorARed >= 40.0 && ColorARed <= 80.0) && (ColorAGreen >= 40.0 && ColorAGreen <= 80.0) && (ColorABlue >= 40.0 && ColorABlue <= 80.0){
                self?.ColorALabel.text = "Dark Gray"
            }
            if (ColorARed >= 130.0 && ColorARed <= 200.0) && (ColorAGreen >= 130.0 && ColorAGreen <= 200.0) && (ColorABlue >= 130.0 && ColorABlue <= 200.0){
                self?.ColorALabel.text = "Gray"
            }
            if (ColorARed >= 100.0 && ColorARed <= 150.0) && (ColorAGreen >= 0.0 && ColorAGreen <= 30.0) && (ColorABlue >= 0.0 && ColorABlue <= 30.0){
                self?.ColorALabel.text = "Maroon"
            }
            if (ColorARed >= 100.0 && ColorARed <= 140.0) && (ColorAGreen >= 100.0 && ColorAGreen <= 140.0) && (ColorABlue >= 0.0 && ColorABlue <= 40.0){
                self?.ColorALabel.text = "Olive"
            }
            if (ColorARed >= 0.0 && ColorARed <= 80.0) && (ColorAGreen >= 200.0 && ColorAGreen <= 225.0) && (ColorABlue >= 0.0 && ColorABlue <= 80.0){
                self?.ColorALabel.text = "Lime"
            }
            if (ColorARed >= 0.0 && ColorARed <= 80.0) && (ColorAGreen >= 0.0 && ColorAGreen <= 80.0) && (ColorABlue >= 100.0 && ColorABlue <= 180.0){
                self?.ColorALabel.text = "Navy"
            }
            
                //colorB-RGB
                let colorAtB = String(describing: image.pixelColor(x: Int(frm1.origin.x), y: Int(frm1.origin.y)))
            
                let ArrayColorB = colorAtB.components(separatedBy: " ")
                
                let colorAtB_R = Float(ArrayColorB[1])
                let colorAtB_G = Float(ArrayColorB[2])
                let colorAtB_B = Float(ArrayColorB[3])
            
                let ColorBRed = Float(colorAtB_R ?? 0.0) * 255
                let ColorBGreen = Float(colorAtB_G ?? 0.0) * 255
                let ColorBBlue = Float(colorAtB_B ?? 0.0) * 255
            
                let IntColorBRed = Int(ColorBRed)
                let IntColorBGreen = Int(ColorBGreen)
                let IntColorBBlue = Int(ColorBBlue)
                
            let IntColorRedFirst = ConvertToHex(IntColor: IntColorBRed, Switch: true)
            let IntColorRedLast = ConvertToHex(IntColor: IntColorBRed, Switch: false)
            let IntColorBBRed = IntColorRedFirst + IntColorRedLast
            
            let IntColorGreenFirst = ConvertToHex(IntColor: IntColorBGreen, Switch: true)
            let IntColorGreenLast = ConvertToHex(IntColor: IntColorBGreen, Switch: false)
            let IntColorBBGreen = IntColorGreenFirst + IntColorGreenLast
            
            let IntColorBlueFirst = ConvertToHex(IntColor: IntColorBBlue, Switch: true)
            let IntColorBlueLast = ConvertToHex(IntColor: IntColorBBlue, Switch: false)
            let IntColorBBBlue = IntColorBlueFirst + IntColorBlueLast
            
            self?.ColorBLabelRGB.text = "#" + IntColorBBRed + IntColorBBGreen + IntColorBBBlue
            
    //            self?.ColorBLabelRGB.text = "R: " + String(Int(ColorBRed)) + " G: " + String(Int(ColorBGreen)) + " B: " + String(Int(ColorBBlue))
                
            //ColorBLabel-Condition
            if (ColorBRed >= 0.0 && ColorBRed <= 40.0) && (ColorBGreen >= 0.0 && ColorBGreen <= 40.0) && (ColorBBlue >= 0.0 && ColorBBlue <= 40.0){
                self?.ColorBLabel.text = "Black"
            }
            if (ColorBRed >= 200.0 && ColorBRed <= 255.0) && (ColorBGreen >= 200.0 && ColorBGreen <= 255.0) && (ColorBBlue >= 200.0 && ColorBBlue <= 255.0){
                self?.ColorBLabel.text = "White"
            }
            if (ColorBRed >= 180.0 && ColorBRed <= 255.0) && (ColorBGreen >= 0.0 && ColorBGreen <= 80.0) && (ColorBBlue >= 0.0 && ColorBBlue <= 80.0){
                self?.ColorBLabel.text = "Red"
            }
            if (ColorBRed >= 140.0 && ColorBRed <= 180.0) && (ColorBGreen >= 0.0 && ColorBGreen <= 80.0) && (ColorBBlue >= 100.0 && ColorBBlue <= 180.0){
                self?.ColorBLabel.text = "Red Violet"
            }
            if (ColorBRed >= 100.0 && ColorBRed <= 140.0) && (ColorBGreen >= 0.0 && ColorBGreen <= 80.0) && (ColorBBlue >= 100.0 && ColorBBlue <= 140.0){
                self?.ColorBLabel.text = "Violet"
            }
            if (ColorBRed >= 0.0 && ColorBRed <= 40.0) && (ColorBGreen >= 0.0 && ColorBGreen <= 40.0) && (ColorBBlue >= 150.0 && ColorBBlue <= 255){
                self?.ColorBLabel.text = "Blue"
            }
            if (ColorBRed >= 0.0 && ColorBRed <= 80.0) && (ColorBGreen >= 100.0 && ColorBGreen <= 180.0) && (ColorBBlue >= 100.0 && ColorBBlue <= 180.0){
                self?.ColorBLabel.text = "Blue Green"
            }
            if (ColorBRed >= 0.0 && ColorBRed <= 80.0) && (ColorBGreen >= 100.0 && ColorBGreen <= 140.0) && (ColorBBlue >= 0.0 && ColorBBlue <= 80.0){
                self?.ColorBLabel.text = "Green"
            }
            if (ColorBRed >= 100.0 && ColorBRed <= 180.0) && (ColorBGreen >= 100.0 && ColorBGreen <= 180.0) && (ColorBBlue >= 0.0 && ColorBBlue <= 80.0){
                self?.ColorBLabel.text = "Yellow Green"
            }
            if (ColorBRed >= 200.0 && ColorBRed <= 255.0) && (ColorBGreen >= 200.0 && ColorBGreen <= 255.0) && (ColorBBlue >= 0.0 && ColorBBlue <= 80.0){
                self?.ColorBLabel.text = "Yellow"
            }
            if (ColorBRed >= 200.0 && ColorBRed <= 255.0) && (ColorBGreen >= 100.0 && ColorBGreen <= 180.0) && (ColorBBlue >= 0.0 && ColorBBlue <= 80.0){
                self?.ColorBLabel.text = "Orange"
            }
            if (ColorBRed >= 40.0 && ColorBRed <= 80.0) && (ColorBGreen >= 40.0 && ColorBGreen <= 80.0) && (ColorBBlue >= 40.0 && ColorBBlue <= 80.0){
                self?.ColorBLabel.text = "Dark Gray"
            }
            if (ColorBRed >= 130.0 && ColorBRed <= 200.0) && (ColorBGreen >= 130.0 && ColorBGreen <= 200.0) && (ColorBBlue >= 130.0 && ColorBBlue <= 200.0){
                self?.ColorBLabel.text = "Gray"
            }
            if (ColorBRed >= 100.0 && ColorBRed <= 150.0) && (ColorBGreen >= 0.0 && ColorBGreen <= 30.0) && (ColorBBlue >= 0.0 && ColorBBlue <= 30.0){
                self?.ColorBLabel.text = "Maroon"
            }
            if (ColorBRed >= 100.0 && ColorBRed <= 140.0) && (ColorBGreen >= 100.0 && ColorBGreen <= 140.0) && (ColorBBlue >= 0.0 && ColorBBlue <= 40.0){
                self?.ColorBLabel.text = "Olive"
            }
            if (ColorBRed >= 0.0 && ColorBRed <= 80.0) && (ColorBGreen >= 200.0 && ColorBGreen <= 225.0) && (ColorBBlue >= 0.0 && ColorBBlue <= 80.0){
                self?.ColorBLabel.text = "Lime"
            }
            if (ColorBRed >= 0.0 && ColorBRed <= 80.0) && (ColorBGreen >= 0.0 && ColorBGreen <= 80.0) && (ColorBBlue >= 100.0 && ColorBBlue <= 180.0){
                self?.ColorBLabel.text = "Navy"
            }
            
                strongSelf.PickerBG.backgroundColor = image.pixelColor(x: Int(frm.origin.x), y: Int(frm.origin.y))
                strongSelf.PickerBBG.backgroundColor = image.pixelColor(x: Int(frm1.origin.x), y: Int(frm1.origin.y))
                
            })
        
        }
        
    
        override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(animated)
            
            navigationController?.setNavigationBarHidden(false, animated: animated)
        }
    
        func cropsToSquare(image: UIImage, targetSize: CGSize) -> UIImage {
            let sizes = image.size
            
            let refWidth = CGFloat(sizes.width)
            let refHeight = CGFloat(sizes.height)
            let cropSizeWidth : CGFloat = 414.0
            let cropSizeHeight : CGFloat = 551.0
            
            print(refWidth)
            print(refHeight)
            
            let x = (refWidth - cropSizeWidth) / 2.0
            let y = (refHeight - cropSizeHeight) / 2.0
            
            let cgimages = image.cgImage
            let cropRect = CGRect(x: x, y: y, width: cropSizeWidth, height: cropSizeHeight)
            print(cropRect)
            let imageRef = cgimages?.cropping(to: cropRect)
            let cropped = UIImage(cgImage: imageRef!, scale: 0.0, orientation: image.imageOrientation)
            
            return cropped
        }
    
        func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
            let size = image.size

            let widthRatio  = targetSize.width  / size.width
            let heightRatio = targetSize.height / size.height

            // Figure out what our orientation is, and use that to form the rectangle
            var newSize: CGSize
            if(widthRatio > heightRatio) {
                newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
            } else {
                newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
            }

            // This is the rect that we've calculated out and this is what is actually used below
            let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)

            // Actually do the resizing to the rect using the ImageContext stuff
            UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
            image.draw(in: rect)
            let newImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()

            return newImage!
        }
        
        @objc func wasDragged (_ gesture: UIPanGestureRecognizer){
            
            let translation = gesture.translation(in: self.view)
            
            contain.center = CGPoint(x: contain.center.x + translation.x, y: contain.center.y + translation.y)
            gesture.setTranslation(CGPoint.zero, in: self.view)
        }
        
        @objc func wasDragged2 (_ gesture: UIPanGestureRecognizer){
                
                let translation = gesture.translation(in: self.view)
                
                containB.center = CGPoint(x: containB.center.x + translation.x, y: containB.center.y + translation.y)
                gesture.setTranslation(CGPoint.zero, in: self.view)
            }
        
    }


    public extension UIImage {

        var pixelWidth: Int {
            return cgImage?.width ?? 0
        }

        var pixelHeight: Int {
            return cgImage?.height ?? 0
        }

        func pixelColor(x: Int, y: Int) -> UIColor {
            assert(
                0..<pixelWidth ~= x && 0..<pixelHeight ~= y,
                "Pixel coordinates are out of bounds")

            guard
                let cgImage = cgImage,
                let data = cgImage.dataProvider?.data,
                let dataPtr = CFDataGetBytePtr(data),
                let colorSpaceModel = cgImage.colorSpace?.model,
                let componentLayout = cgImage.bitmapInfo.componentLayout
            else {
                assertionFailure("Could not get a pixel of an image")
                return .clear
            }

            assert(
                colorSpaceModel == .rgb,
                "The only supported color space model is RGB")
            assert(
                cgImage.bitsPerPixel == 32 || cgImage.bitsPerPixel == 24,
                "A pixel is expected to be either 4 or 3 bytes in size")

            let bytesPerRow = cgImage.bytesPerRow
            let bytesPerPixel = cgImage.bitsPerPixel/8
            let pixelOffset = y*bytesPerRow + x*bytesPerPixel

            if componentLayout.count == 4 {
                let components = (
                    dataPtr[pixelOffset + 0],
                    dataPtr[pixelOffset + 1],
                    dataPtr[pixelOffset + 2],
                    dataPtr[pixelOffset + 3]
                )

                var alpha: UInt8 = 0
                var red: UInt8 = 0
                var green: UInt8 = 0
                var blue: UInt8 = 0

                switch componentLayout {
                case .bgra:
                    alpha = components.3
                    red = components.2
                    green = components.1
                    blue = components.0
                case .abgr:
                    alpha = components.0
                    red = components.3
                    green = components.2
                    blue = components.1
                case .argb:
                    alpha = components.0
                    red = components.1
                    green = components.2
                    blue = components.3
                case .rgba:
                    alpha = components.3
                    red = components.0
                    green = components.1
                    blue = components.2
                default:
                    return .clear
                }

                // If chroma components are premultiplied by alpha and the alpha is `0`,
                // keep the chroma components to their current values.
                if cgImage.bitmapInfo.chromaIsPremultipliedByAlpha && alpha != 0 {
                    let invUnitAlpha = 255/CGFloat(alpha)
                    red = UInt8((CGFloat(red)*invUnitAlpha).rounded())
                    green = UInt8((CGFloat(green)*invUnitAlpha).rounded())
                    blue = UInt8((CGFloat(blue)*invUnitAlpha).rounded())
                }

                return .init(red: red, green: green, blue: blue, alpha: alpha)

            } else if componentLayout.count == 3 {
                let components = (
                    dataPtr[pixelOffset + 0],
                    dataPtr[pixelOffset + 1],
                    dataPtr[pixelOffset + 2]
                )

                var red: UInt8 = 0
                var green: UInt8 = 0
                var blue: UInt8 = 0

                switch componentLayout {
                case .bgr:
                    red = components.2
                    green = components.1
                    blue = components.0
                case .rgb:
                    red = components.0
                    green = components.1
                    blue = components.2
                default:
                    return .clear
                }

                return .init(red: red, green: green, blue: blue, alpha: UInt8(255))

            } else {
                assertionFailure("Unsupported number of pixel components")
                return .clear
            }
        }

    }

    public extension UIColor {

        convenience init(red: UInt8, green: UInt8, blue: UInt8, alpha: UInt8) {
            self.init(
                red: CGFloat(red)/255,
                green: CGFloat(green)/255,
                blue: CGFloat(blue)/255,
                alpha: CGFloat(alpha)/255)
        }

    }

    public extension CGBitmapInfo {

        enum ComponentLayout {

            case bgra
            case abgr
            case argb
            case rgba
            case bgr
            case rgb

            var count: Int {
                switch self {
                case .bgr, .rgb: return 3
                default: return 4
                }
            }

        }

        var componentLayout: ComponentLayout? {
            guard let alphaInfo = CGImageAlphaInfo(rawValue: rawValue & Self.alphaInfoMask.rawValue) else { return nil }
            let isLittleEndian = contains(.byteOrder32Little)

            if alphaInfo == .none {
                return isLittleEndian ? .bgr : .rgb
            }
            let alphaIsFirst = alphaInfo == .premultipliedFirst || alphaInfo == .first || alphaInfo == .noneSkipFirst

            if isLittleEndian {
                return alphaIsFirst ? .bgra : .abgr
            } else {
                return alphaIsFirst ? .argb : .rgba
            }
        }

        var chromaIsPremultipliedByAlpha: Bool {
            let alphaInfo = CGImageAlphaInfo(rawValue: rawValue & Self.alphaInfoMask.rawValue)
            return alphaInfo == .premultipliedFirst || alphaInfo == .premultipliedLast
        }

    }




