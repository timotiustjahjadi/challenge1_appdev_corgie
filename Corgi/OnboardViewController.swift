//
//  OnboardViewController.swift
//  Corgi
//
//  Created by Christopher Kevin Susandji on 29/03/20.
//  Copyright © 2020 artitude. All rights reserved.
//

import UIKit

class OnboardViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    
    
    @IBOutlet weak var getStartedButton: UIButton!
    
    @IBAction func getStarted(_ sender: Any) {
    
        
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let mainVC = storyboard.instantiateViewController(withIdentifier: "mainVC")
        mainVC.modalPresentationStyle = .fullScreen
               
        self.present(mainVC, animated: true, completion: nil)
       
        
        
      //  getStartedButton.layer.cornerRadius = getStartedButton.frame.height / 3
        
    }

}
