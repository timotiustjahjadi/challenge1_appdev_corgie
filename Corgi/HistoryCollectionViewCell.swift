//
//  HistoryCollectionViewCell.swift
//  Corgi
//
//  Created by Christopher Kevin Susandji on 29/03/20.
//  Copyright © 2020 artitude. All rights reserved.
//

import UIKit

class HistoryCollectionViewCell: UICollectionViewCell {
     @IBOutlet weak var myImage: UIImageView!
        @IBOutlet weak var checkmarkLabel: UIImageView!

    //    A Property Observer is created which will toggle the visibility of the checkmark label according if the collection view controller is in editing mode or not.
        var isInEditingMode: Bool = false {
            didSet {
                self.isSelected = false
                checkmarkLabel.isHidden = !isInEditingMode
                
            }
        }
        
    //    Another property observer is created which will display/remove the checkmark when the cell is selected or not.
        override var isSelected: Bool {
        didSet {
            if isInEditingMode {
    //            checkmarkLabel.text = isSelected ? "✓" : ""
                let check = UIImage(systemName: "checkmark.circle.fill")!
                checkmarkLabel.image = isSelected ? check : nil
                checkmarkLabel.backgroundColor = isSelected ? UIColor.white : nil
                checkmarkLabel.layer.cornerRadius = checkmarkLabel.frame.height/2
                 
                

                
            }
        }
    }
}
